# Error correcting codes for Images

Using two different methods (Hamming and BCH) to correct potential transmitted errors on images. 

The results can be seen in the results directory. 

I used a gandalf picture in which I introduced random errors which were then detected and corrected by both BCH and Hamming algorithms. 

You have also access to a comparison of the computed time of both algorithms in function of the size of a picture. 